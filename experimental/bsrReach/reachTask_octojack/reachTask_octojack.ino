#include <MultiStepper.h>
#include <AccelStepper.h>
#include <Servo.h> 


#define SERVO0 6
#define SERVO1 5
#define SERVO2 4
#define SERVO3 3

#define STEPPER0_DIR  7 
#define STEPPER0_STEP 8
#define STEPPER0_NEN  9
#define STEPPER1_DIR  10
#define STEPPER1_STEP 11
#define STEPPER1_NEN  12
#define STEPPER2_DIR  24
#define STEPPER2_STEP 25
#define STEPPER2_NEN  26

#define GPIO0 29
#define GPIO1 30
#define GPIO2 31
#define GPIO3 32
#define GPIO4 35
#define GPIO5 36
#define GPIO6 A21
#define GPIO7 A22

#define DCMOTOR_PWMA 14
#define DCMOTOR_AI1  16
#define DCMOTOR_AI2  15
#define DCMOTOR_BI1  18
#define DCMOTOR_BI2  19
#define DCMOTOR_PWMB 20
#define DCMOTOR_STBY 17

#define RELAY0 39
#define RELAY1 38
#define RELAY2 37

#define LED0 21
#define LED1 22

Servo servo0, servo1, servo2, servo3;

int nBytes;
char comBuf[200];


void setup() {
  // put your setup code here, to run once:
  pinMode(SERVO0, OUTPUT);
  pinMode(SERVO1, OUTPUT);
  pinMode(SERVO2, OUTPUT);
  pinMode(SERVO3, OUTPUT);

  pinMode(STEPPER0_DIR, OUTPUT);
  pinMode(STEPPER0_STEP, OUTPUT);
  pinMode(STEPPER0_NEN, OUTPUT);
  pinMode(STEPPER1_DIR, OUTPUT);
  pinMode(STEPPER1_STEP, OUTPUT);
  pinMode(STEPPER1_NEN, OUTPUT);
  pinMode(STEPPER2_DIR, OUTPUT);
  pinMode(STEPPER2_STEP, OUTPUT);
  pinMode(STEPPER2_NEN, OUTPUT);  
  digitalWrite(STEPPER0_NEN, HIGH);  
  digitalWrite(STEPPER1_NEN, HIGH);  
  digitalWrite(STEPPER2_NEN, HIGH);  

  pinMode(DCMOTOR_PWMA, OUTPUT);
  pinMode(DCMOTOR_AI1, OUTPUT);
  pinMode(DCMOTOR_AI2, OUTPUT);
  pinMode(DCMOTOR_BI1, OUTPUT);
  pinMode(DCMOTOR_BI2, OUTPUT); 
  pinMode(DCMOTOR_PWMB, OUTPUT);
  pinMode(DCMOTOR_STBY, OUTPUT);

  pinMode(RELAY0, OUTPUT);
  pinMode(RELAY1, OUTPUT);
  pinMode(RELAY2, OUTPUT);

  pinMode(LED0, OUTPUT);
  pinMode(LED1, OUTPUT);


  pinMode(GPIO0, INPUT);
  /*  
  pinMode(GPIO1, ??);
  pinMode(GPIO2, ??);
  pinMode(GPIO3, ??);
  pinMode(GPIO4, ??);
  pinMode(GPIO5, ??);
  pinMode(GPIO6, ??);
  pinMode(GPIO7, ??);
  */

 
  servo0.attach(SERVO0);
  servo1.attach(SERVO1);
  servo2.attach(SERVO2);
  servo3.attach(SERVO3);
  servo0.write(100);
  servo1.write(50);
  servo2.write(50);

  Serial.begin(115200);
  nBytes = 0;
}




void loop() {

  int x = touchRead(GPIO0);
  Serial.println(x);
  delay(5);

  if (Serial.available()){
    comBuf[nBytes] = Serial.read();
    nBytes++;
    if (comBuf[nBytes-1] == '\n'){
      int x0,x1,x2;
      sscanf(comBuf, "%d,%d,%d\n", &x0, &x1, &x2);
      servo0.write(x0);
      servo1.write(x1);
      servo2.write(x2);
      nBytes=0;
    }
  }


}
