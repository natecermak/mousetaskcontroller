int touchPins[] = {15,16};
unsigned long mainLoopTime = 5000; // sets sampling rate. default is 200 Hz (5000 us).
elapsedMicros sinceLastLoop;
char sendBuf[200];

void setup() {
  Serial.begin(9600);

  pinMode(touchPins[0], INPUT);
  pinMode(touchPins[1], INPUT);
}

void loop() {
  if (sinceLastLoop > mainLoopTime) {
    sinceLastLoop = sinceLastLoop - mainLoopTime;
    int n = sprintf(sendBuf, "%lu,%d,%d\n", millis(), touchRead(touchPins[0]), touchRead(touchPins[1]));
    Serial.write(sendBuf,n);
  }
}
