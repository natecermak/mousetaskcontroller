/* BSRcontroller.ino. (c) Nathan Cermak 2018. 
 *      Run brain-stimulation reward for a single animal
 * Receives a trigger command via serial interface, immediately 
 * begins a pulse train of specified duration and frequency on 
 * digital pin 6 (e.g. to drive a stimulator). 
 * 
 * Serial commands:
 *    s\n       start stimulus
 *    f100\n    set frequency to 100 Hz
 *    d300\n    set duration of pulse train to 300 ms. 
 */
 
int triggerPin = 6;
unsigned long del = 5000;          // microseconds (default is 5000

float alpha = 0.5, alpha_ = 0.001; // per sample (defaults are 0.5, 0.001)
unsigned long trainLength = 500;   // milliseconds (default is 500);
int thresh = 100;                  // Capacitive units (?) (Default is 100)
int stimFreq = 150; // Hz
int loopIter = 0;

char *comBuf;
int nChar;

int lastBroadcast;
IntervalTimer stimTimer0;
elapsedMicros sinceLastLoop;
unsigned long trainStartTime = 0;

void triggerStimulus0() {
  if (millis() - trainStartTime <= trainLength) {
    digitalWrite(triggerPin, HIGH);
    delay(2);
    digitalWrite(triggerPin, LOW);
  }  else {
    stimTimer0.end();
  }
}

void setup() {
  Serial.begin(9600);
  stimTimer0.priority(0);
  pinMode(triggerPin, OUTPUT);
  trainStartTime = millis() - trainLength;
  lastBroadcast = millis();
  comBuf = (char*) malloc(24);
  nChar = 0;
}


void loop() {
  if (Serial.available() > 0) {
    Serial.readBytes(comBuf + nChar, 1); // read one byte into the buffer
    nChar++; // keep track of the number of characters we've read!

    if (comBuf[nChar - 1] == '\n') { // termination character for string - means we've recvd a full command!
      comBuf[nChar] = '\0'; // be safe!
      
      if (comBuf[0] == 's') {
        trainStartTime = millis();
        stimTimer0.begin(triggerStimulus0, 1e6 / stimFreq);
      }
      if (comBuf[0] == 'f') {
        stimFreq = atoi(comBuf + 1);
        Serial.print("Set FREQ to ");  Serial.println(stimFreq);
      }
      if (comBuf[0] == 'd'){
        trainLength = atoi(comBuf + 1);        
        Serial.print("Set DURATION to ");  Serial.println(trainLength);
      }
      nChar = 0; // reset the pointer!
    }
  }

}
