# Mouse Task Controller

Contains Labview VIs and microcontroller code for operating a handreach task (a la Hantman group), or simple treadmill running during two-photon imaging. 

Dependencies for microcontroller code include the Teensyduino add-on to the Arduino IDE, and associated libraries. 

The Labview VIs require at least Labview 2018, the Vision Development Module, and the Vision Acquisition Software.

The programs expect two 0.4Mpx Blackfly S USB3 Cameras (FLIR/Point Grey BFS-U3-04S2C-CS).